<?php

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('AdminModel');
        $this->load->model('UserModel');
        if (!isset($this->session->userdata['uid'])) {
            redirect('login');
        }
    }

    public function index() {


        $res['totNews'] = $this->AdminModel->totNews();
        $res['title'] = 'Home';
        $this->load->view('admin/index', $res);
    }

    //***************** Profile *********************//
    public function profile() {
        $res['title'] = 'Profile';
        $this->load->view('admin/profile', $res);
    }

    

    public function profileChange() {
        $data = array('userName' => trim($this->input->post('name')),
            'phone' => trim($this->input->post('phone')),
            'email' => trim($this->input->post('email')));
        $id = $this->session->userdata('uid');
        $status = $this->AdminModel->profileSave($data, $id);
        if ($status == 1) {
            $sessData = array(
                'name' => $data['userName'],
                'email' => $data['email'],
                'phone' => $data['phone']
                );
            $this->session->set_userdata($sessData);
        }
        redirect('admin/profile');
    }

    public function settings() {
        $res['title'] = 'Settings';
        $this->load->view('admin/settings', $res);
    }

    //******************** Settings **************************
    public function pwdCheck() {
        $pwd = trim($this->input->post('pwd'));

        $pwdhash = $this->UserModel->hash($pwd);
        $id = $this->session->userdata('uid');
        $res = $this->AdminModel->chkPwd($pwdhash, $id);
        if (empty($res)) {
            $data['valid'] = 0;
        } else {
            $data['valid'] = 1;
        }
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function pwdChange() {
        $pwd = trim($this->input->post('new'));
        $id = $this->session->userdata('uid');
        $pwdhash = $this->UserModel->hash($pwd);
        $num = $this->AdminModel->chgPwd($id, $pwdhash);
        if ($num > 0) {
            $res['status'] = 1;
        } else {
            $res['status'] = 0;
        }
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($res));
    }

    //******************** News **************************
    public function news($start = 0) {
        $res = $this->getNews($start);
        $this->load->view('admin/news', $res);
    }

    
// Create post
    public function post() {
        $news = array('title' => trim($this->input->post('title')),
            'content' => trim($this->input->post('details')),
            'size' => trim($this->input->post('size')),
            'color' => trim($this->input->post('color')),
            'poster' =>$this->session->userdata('name')
            );

        $config['upload_path'] = './uploads/posts';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = 'default';
        $config['max_size'] = '6144';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $result = array('error' => $this->upload->display_errors('<span>', '</span>'));
        } else {
            $id = $this->AdminModel->newsData($news);
            $d = $this->upload->data();
            $img = $this->resizeImg($id, $d);
            $this->AdminModel->addNewsImage($id, $img);
            $result['success'] = 'success';
        }
        $res = $this->getNews();
        $resp = array_merge($res, $result);
        $this->load->view('admin/news', $resp);
    }

// Get paginated posts
    private function getNews($start = 0) {
        $res['title'] = 'Blog | Occudiz';
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/admin/news';
        $config['total_rows'] = $this->UserModel->newsCount();
        $config['per_page'] = 5;
        $this->pagination->initialize($config);
        $res['news'] = $this->UserModel->getNews(5, $start);
        $res['pages'] = $this->pagination->create_links();
        return $res;
    }
// Resize and rename image
    private function resizeImg($id, $d) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = './uploads/posts/' . $d["file_name"];
        $config['maintain_ratio'] = TRUE;
        // $config['width'] = 480;
        // $config['height'] = 250;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $img = 'post_' . $id . $d["file_ext"];
        rename('./uploads/posts/' . $d["file_name"], './uploads/posts/' . $img);
        return $img;
    }
// Delete post
    public function delPost($id) {
        $this->AdminModel->delPost('posts', array('postId' => $id));
        $res = $this->getNews();
        $resp['delete'] = 1;
        $resp = array_merge($res, $resp);
        $this->load->view('admin/news', $resp);
    }
    //******************** Career **************************
    private function getCareers($start = 0) {
        $career['title'] = 'Careers';
        $config['base_url'] = site_url() . '/admin/career';
        $config['total_rows'] = $this->UserModel->careerCount();
        $config['per_page'] = 5;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $career['careers'] = $this->UserModel->getCareer(5, $start);
        $career['pages'] = $this->pagination->create_links();

        return $career;
    }

    public function careerPost() {
        $career = array('jobtitle' => $this->input->post('job'),
            'division' => $this->input->post('div'),
            'company' => $this->input->post('comp'),
            'location' => $this->input->post('loc'),
            'eligibility' => $this->input->post('qual'),
            'contactnum' => $this->input->post('cnum'),
            'contactmail' => $this->input->post('cemail'),
            'description' => $this->input->post('details')
            );
        $id = $this->AdminModel->newCareer($career);
        if ($id > 0) {
            $res['success'] = 1;
        } else {
            $res['error'] = 1;
        }
        $resl = $this->getCareers();
        $resp = array_merge($resl, $res);
        $this->load->view('admin/career', $resp);
    }

    public function career($start = 0) {
        $res = $this->getCareers($start);
        $this->load->view('admin/career', $res);
    }

    public function delCareer($id) {
        $this->AdminModel->delPost('careers', array('careerId' => $id));
        $res = $this->getCareers();
        $resp['delete'] = 1;
        $resp = array_merge($res, $resp);

        $this->load->view('admin/career', $resp);
    }

    //******************** prtfoloio ***************************//      
    public function portfolio($start = 0) {
        $res = $this->getPortItems($start);
        $this->load->view('admin/portfolio', $res);
    }

    private function getPortItems($start = 0) {
        $res['title'] = 'Portfolio';
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/admin/portfolio';
        $config['total_rows'] = $this->UserModel->imgCount();
        $config['per_page'] = 5;
        $this->pagination->initialize($config);
        $res['img'] = $this->UserModel->getImg(5, $start);
        $res['pages'] = $this->pagination->create_links();
        return $res;
    }

    public function addImage() {
        $portfolioImgage = array('title' => trim($this->input->post('title')),
            'description' => trim($this->input->post('desc')),
            'details' => trim($this->input->post('details')),
            'type' => 'image', 'link' => 'noimage.jpg'
            );
        $config['upload_path'] = './uploads/portfolio';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = 'default';
        $config['max_size'] = '1024';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $result = array('error' => $this->upload->display_errors('<span>', '</span>'));
        } else {
            $id = $this->AdminModel->portfolioImg($portfolioImgage);
            $d = $this->upload->data();
            $img = $this->imgResize($id, $d, 570, 356, 'portfolio', 'portfolio');
            $this->AdminModel->addPortImage($id, $img);
            $result['success'] = 'success';
        }
        $res = $this->getPortItems();
        $resp = array_merge($res, $result);
        $this->load->view('admin/portfolio', $resp);
    }

    private function imgResize($id, $d, $width, $height, $prefix, $dir) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = './uploads/' . $dir . '/' . $d["file_name"];
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $img = $prefix . $id . $d["file_ext"];
        rename('./uploads/' . $dir . '/' . $d["file_name"], './uploads/' . $dir . '/' . $img);
        return $img;
    }

    public function delPortfolio($id) {
        $this->AdminModel->delPost('portfolio', array('contentId' => $id));
        $res = $this->getPortItems();
        $resp['delete'] = 1;
        $resp = array_merge($res, $resp);

        $this->load->view('admin/portfolio', $resp);
    }

    /*     * ***********************Video********************** */
    
    public function video($start=0) {
        $res = $this->getVideo($start);
        $this->load->view('admin/video', $res);

    }
    


    private function getVideo($start=0) {
        $data['title'] = 'Video';
        $config['base_url'] = site_url() . '/admin/video';
        $config['total_rows'] = $this->UserModel->videoCount();
        $config['per_page'] = 5;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data['video'] = $this->UserModel->getVideo(5, $start);
        $data['pages'] = $this->pagination->create_links();

        return $data;
    }

    public function addVideo() {
        $video = array('title' => trim($this->input->post('title')),
            'link' => $this->input->post('video'),
            'details' => $this->input->post('details'),
            );
        
        $id = $this->AdminModel->newVideo($video);
        if ($id > 0) {
            $res['success'] = 1;
        } else {
            $res['error'] = 1;
        }
        $resl = $this->getVideo();
        $resp = array_merge($resl, $res);
        $this->load->view('admin/video', $resp);
    }
    
    
    public function delVideo($id) {
        $this->AdminModel->delPost('video', array('videoID' => $id));
        $res = $this->getVideo();
        $resp['delete'] = 1;
        $resp = array_merge($res, $resp);

        $this->load->view('admin/video', $resp);
    }

    /*     * *************************testimonials*************************** */

    public function testimonials($start = 0) {
        $res = $this->getTest($start);
        $this->load->view('admin/testimonials', $res);
    }

    private function getTest($start = 0) {
        $res['title'] = 'Testimonials';
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/admin/testimonials';
        $config['total_rows'] = $this->UserModel->testCount();
        $config['per_page'] = 5;
        $this->pagination->initialize($config);
        $res['test'] = $this->UserModel->getTest(5, $start);
        $res['pages'] = $this->pagination->create_links();
        return $res;
    }

    public function postTest() {
        $test = array('name' => $this->input->post('name'),
            'designation' => $this->input->post('designation'),
            'quote' => $this->input->post('quote'));
        $config['upload_path'] = './uploads/testimonials';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['file_name'] = 'default';
        $config['max_size'] = '1024';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $result = array('error' => $this->upload->display_errors('<span>', '</span>'));
        } else {
            $id = $this->AdminModel->newTest($test);
            $d = $this->upload->data();
            $img = $this->imgResize($id, $d, 270, 270, 'test', 'testimonials');
            $this->AdminModel->addTestImage($id, $img);
            $result['success'] = 'success';
        }
        $res = $this->getTest();
        $resp = array_merge($res, $result);
        $this->load->view('admin/testimonials', $resp);
    }

    public function delTest($id) {
        $this->AdminModel->delPost('testimonials', array('testID' => $id));
        $res = $this->getTest();
        $resp['delete'] = 1;
        $resp = array_merge($res, $resp);

        $this->load->view('admin/testimonials', $resp);
    }

    /*     * ***************logout************************ */

    public function logout() {
        $this->session->sess_destroy(); {
            $this->load->view('login');
        }
    }

    /*     * *******************startups******************** */

    public function startups($start = 0) {
        $res = $this->getStart($start);
        $this->load->view('admin/startups', $res);
    }

    private function getStart($start = 0) {
        $res['title'] = 'Startups';
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/admin/startups';
        $config['total_rows'] = $this->UserModel->testCount();
        $config['per_page'] = 5;
        $this->pagination->initialize($config);
        $res['start'] = $this->UserModel->getStart(5, $start);
        $res['pages'] = $this->pagination->create_links();
        return $res;
    }

    public function poststart() {
        $test = array('name' => $this->input->post('name'),
            'details' => $this->input->post('details'),
            'website' => $this->input->post('website'));
        $config['upload_path'] = './uploads/startups';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['file_name'] = 'default';
        $config['max_size'] = '10240';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('logo')) {
            $result = array('error' => $this->upload->display_errors('<span>', '</span>'));
        } else {
            $id = $this->AdminModel->newStart($test);
            $d = $this->upload->data();
            $img = $this->imgResize($id, $d, 300, 300, 'start', 'startups');
            $this->AdminModel->addStartImage($id, $img);
            $result['success'] = 'success';
        }
        $res = $this->getStart();
        $resp = array_merge($res, $result);
        $this->load->view('admin/startups', $resp);
    }

    public function delStart($id) {
        $this->AdminModel->delPost('startups', array('startID' => $id));
        $res = $this->getStart();
        $resp['delete'] = 1;
        $resp = array_merge($res, $resp);

        $this->load->view('admin/startups', $resp);
    }

    # Add faq 
    public function addfaq(){
        $faq = array(
            'question' => trim($this->input->post('ques')),
            'answer' => trim($this->input->post('ans')),
            'answeredby' => trim($this->session->userdata('name')),
            );
        $id = $this->AdminModel->addFAQ($faq);
        $res = $this->getFaq();
        if ($id > 0) {
            $res['success'] = 1;
        } else {
            $res['error'] = 1;
        }
        $this->load->view('admin/faq', $res);
    }

     # View faq
    public function FAQ($start = 0 ){
        $res = $this->getFaq($start);
        $this->load->view('admin/faq', $res);
    }

    #Get paginated faq post
    public function getFaq($start = 0){
        $res['title'] = 'FAQ | Occudiz';
        $this->load->library('pagination');
        $config['base_url'] = site_url() . '/admin/FAQ';
        $config['total_rows'] = $this->UserModel->faqCount();
        $config['per_page'] = 5;
        $this->pagination->initialize($config);
        $res['faq'] = $this->UserModel->getFaq(5, $start);
        $res['pages'] = $this->pagination->create_links();
        return $res;
    }

    # deleteFAQ
    public function delFaq($id) {
        $this->AdminModel->delPost('questions', array('qiD' => $id));
        $res = $this->getFaq();
        $res['delete'] = 1;
        $this->load->view('admin/faq', $res);
    }

}
