<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		$this->load->model('AdminModel');
		$this->load->model('UserModel');
	}

	public function index(){
		$res = $this->getNews(0);
		$this->load->view('blog', $res);
	}

// Blog 
	public function readblog($start = 0)
	{
		$res = $this->getNews($start);
		$this->load->view('blog', $res);
	}
// Read single blog
	public function read($id = null)
	{	if(!$id)
		redirect('blog/index') ;  
		$res = $this->getNews();
		$res['post'] = $this->AdminModel->getNews($id);
		$this->load->view('single', $res);
	}
//Get posts
	public function news($start = 0) {
		$res = $this->getNews($start);
		$this->load->view('admin/news', $res);
	}
// Get paginated posts
	private function getNews($start = 0) {
		$res['title'] = 'News';
		$this->load->library('pagination');
		$config['base_url'] = site_url() . '/admin/news';
		$config['total_rows'] = $this->UserModel->newsCount();
		$config['per_page'] = 5;
		$this->pagination->initialize($config);
		$res['news'] = $this->UserModel->getNews(5, $start);
		$res['pages'] = $this->pagination->create_links();
		return $res;
	}

}
