<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FAQ extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('AdminModel');
		$this->load->model('UserModel');
	}

  # View faq
	public function index($start = 0 ){
		$res = $this->getFaq($start);
		$res['colors'] = ['#FFA500','#cc0000','#009933','#993366'];
		$res['blog'] = $this->UserModel->getNews(2, 0);
		$this->load->view('faq', $res);
	}

    #Get paginated faq post
	public function getFaq($start = 0){
		$res['title'] = 'FAQ | Occudiz';
		$this->load->library('pagination');
		$config['base_url'] = site_url() . '/admin/FAQ';
		$config['total_rows'] = $this->UserModel->faqCount();
		$config['per_page'] = 5;
		$this->pagination->initialize($config);
		$res['faq'] = $this->UserModel->getFaq(5, $start);
		$res['pages'] = $this->pagination->create_links();
		return $res;
	}


}
