<?php

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
    }

    public function index() {

        $this->load->view('login');
    }

    public function doLogin() {
        $usrCredentials = array(
            'name' => $this->input->post('name'),
            'password' => $this->input->post('pwd')
        );
        $response = array('url' => 'false', 'error' => 'Invalid user credentials');
        if (isset($usrCredentials['name']) || isset($usrCredentials['password'])) {
            $usrCredentials['password'] = $this->UserModel->hash($usrCredentials['password']);
            $userData = $this->UserModel->attemptLogin($usrCredentials);
            if (!empty($userData)) {
                $data = array(
                    'uid' => $userData['userId'],
                    'name' => $userData['userName'],
                    'email' => $userData['email'],
                    'phone' => $userData['phone']
                );
                $this->session->set_userdata($data);
                $response['url'] = site_url() . '/admin';
            }
        } else {
            redirect('home/login');
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

    public function forgotPwd() {
        $mail = trim($this->input->post('mail'));
        $user = $this->UserModel->mailChk($mail);
        if (!empty($user)) {
            $new = random_string($type = 'alnum', 8) . substr($mail, 0, 5);
            $pwd = $this->UserModel->hash($new);
            $count = $this->UserModel->forgot($user['userId'], $pwd);
            $json['status'] = $count;
            if ($count > 0) {
                        $headers = "MIME-Version: 1.0" . "\r\n";
						$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
						$headers .="From:Occudiz <support@Occudiz.com>" . "\r\n";
						$to = $mail;
						$subject = "Password Reset";
						$message = "<html><body><h3 style=\"color:#38A5DD\">Ocudiz</h3><div>Hi <strong>{$user['userName']}</strong>,<p>You recently requested a password reset.<br/>Your new password is : <b>{$new}</b></p></div></body></html>";
                mail($to, $subject, $message, $headers);
            }
        } else {
            $json['status'] = 0;
        }
        $this->output
                ->set_content_type('application.json')
                ->set_output(json_encode($json));
    }

}
