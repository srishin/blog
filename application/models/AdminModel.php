<?php

class AdminModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //******************** Profile ***********************//
    public function profileSave($data, $id) {
        $this->db->update('users', $data, array('userId' => $id));
        return $this->db->affected_rows();
    }

    //******************** Settings ***********************//
    public function chkPwd($pwd, $id) {
        return $this->db->get_where('users', array('userId' => $id, 'password' => $pwd))->row_array();
    }

    public function chgPwd($id, $pwd) {
        $data['password'] = $pwd;
        $this->db->update('users', $data, array('userId' => $id));
        return $this->db->affected_rows();
    }
    
    //************************* News ***************************//
    public function newsData($news){
        $this->db->insert('posts',$news);
        return $this->db->insert_id();
    }   
    public function getNews($id){
     return $this->db->get_where('posts',array('postId'=>$id))->row_array();
 }
 public function addNewsImage($id,$img){
    $this->db->update('posts',array('img'=>$img),array('postId'=>$id));
}
public function totNews() {
    return $this->db->count_all_results('posts');
}
/*----------------- FAQ ------------------*/
    #insertFAQ
public function addFAQ($faq){
 $this->db->insert('questions',$faq);
 return $this->db->insert_id();
}



    //********************** Career ********************** //
public function newCareer($career){
    $this->db->insert('careers',$career);
    return $this->db->affected_rows();
}
public function totCar() {
    return $this->db->count_all_results('careers');
}
    //******************* Delete Posts **************************//
public function delPost($table,$array) {
    $this->db->delete($table,$array);
}

    //******************** Portfolio ******************//
public function portfolioImg($img){
    $this->db->insert('portfolio',$img);
    return $this->db->insert_id();
}
public function addPortImage($id, $img){
    $this->db->update('portfolio',array('link'=>$img),array('contentId'=>$id));
}
public function totPort() {
    return $this->db->count_all_results('portfolio');
}
/*********************Testimonials**************************/
public function newTest($test){
    $this->db->insert('testimonials',$test);
    return $this->db->insert_id();
}
public function addTestImage($id, $img){
    $this->db->update('testimonials',array('image'=>$img),array('testID'=>$id));
}
public function totTest() {
    return $this->db->count_all_results('testimonials');
}

/*********************startups*************************/


public function newStart($start){
    $this->db->insert('startups',$start);
    return $this->db->insert_id();
}
public function addStartImage($id, $logo){
    $this->db->update('startups',array('logo'=>$logo),array('startID'=>$id));
}
public function totStart() {
    return $this->db->count_all_results('startups');
}

/************video**************/
public function newVideo($video){
    $this->db->insert('video',$video);
    return $this->db->affected_rows();
}




public function addVideo($id, $video){
    $this->db->update('video',array('link'=>$video),array('videoID'=>$id));
}
public function totVideo() {
    return $this->db->count_all_results('video');
}

}

