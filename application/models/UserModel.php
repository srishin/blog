<?php

class UserModel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function newSubscriber($sub) {
        $this->db->insert('subscribers', $sub);
        return $this->db->affected_rows();
    }

    public function getNews($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->order_by('date', 'desc')
        ->get_where('posts', array('status' => 1))
        ->result_array();
    }    


    public function newsCount() {
        $rows = $this->db
        ->get_where('posts', array('status' => 1))
        ->result_array();
        return count($rows);
    }

    public function latestNews() {
        return $this->db
        ->select('title')
        ->limit(6, 0)
        ->order_by('date', 'desc')
        ->get_where('news', array('status' => 1))
        ->result_array();
    }

     # paginatedFaq
    public function getFaq($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->order_by('date', 'desc')
        ->get_where('questions', array('status' => 1))
        ->result_array();
    }

    # Total no of questions
    public function faqCount() {
        return $this->db->count_all_results('questions');
    }

    //********************* Login ****************************//
    function attemptLogin($usrCredentials) {
        $user = $usrCredentials['name'];
        $pwd = $usrCredentials['password'];
        $id = $this->db->get_where('users', array('userName' => $user, 'password' => $pwd))->row_array();
        return ($id);
    }

    public function loggedin() {
        return ($this->session->userdata('name'));
    }

    public function logout($id) {
        $this->session->sess_destroy();
    }

    public function forgot($id, $new) {
        $data['password'] = $new;
        $this->db->update('users', $data, array('userId' => $id['userId']));
        return $this->db->affected_rows();
    }

    public function hash($string) {//Hashing Function
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function mailChk($mail) {
        $data = $this->db->get_where('users', array('email' => $mail))->row_array();
        return $data;
    }

    //*************** Career ********************//

    public function getCareer($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->order_by('careerDate', 'desc')
        ->get_where('careers', array('status' => 1))
        ->result_array();
    }

    public function careerCount() {
        $rows = $this->db
        ->get_where('careers', array('status' => 1))
        ->result_array();
        return count($rows);
    }

    //******************* Portfolio ***********************//
    public function imgCount() {
        $rows = $this->db
        ->get_where('portfolio', array('status' => 1))
        ->result_array();
        return count($rows);
    }

    public function getImg($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->order_by('created', 'desc')
        ->get_where('portfolio', array('status' => 1))
        ->result_array();
    }

    /*     * *************************testimonials************************ */

    public function testCount() {
        $rows = $this->db
        ->get_where('testimonials', array('status' => 1))
        ->result_array();
        return count($rows);
    }

    public function getTest($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->order_by('created', 'desc')
        ->get_where('testimonials', array('status' => 1))
        ->result_array();
    }

    /*     * *************************Startups******************************** */

    public function StartCount() {
        $rows = $this->db
        ->get_where('startups', array('status' => 1))
        ->result_array();
        return count($rows);
    }

    public function getStart($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->order_by('created', 'desc')
        ->get_where('startups', array('status' => 1))
        ->result_array();
    }

    public function getStartups($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->get_where('startups', array('status' => 1))
        ->result_array();
    }

    public function videoCount() {
        $rows = $this->db
        ->get('video')
        ->result_array();
        return count($rows);
    }

    public function getVideo($limit, $start) {
        return $this->db
        ->limit($limit, $start)
        ->order_by('created', 'desc')
        ->get('video')
        ->result_array();
    }

}
