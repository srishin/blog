<?php require_once 'home.php'; ?>              
<div class="span9">
    <h2> <span class="vc_main-color"> Careers </span> </h2>
    <?php
    if (!empty($error)) {
        echo '<div class="alert alert-error"> ' . $error . '<i class="icon-exclamation-sign"> </i></div>';
    }
    if (!empty($success)) {
        echo '<div class="alert alert-success"><i class="icon-ok-sign"></i> New news item was added successfully</div>';
    }
    if (!empty($delete)) {
        echo '<div class="alert alert-error"><i class="icon-ok-sign"></i> Deleted successfully</div>';
    }
    ?>
    <a class="toggle-link" href="#Add-Career"><i class="icon-plus"></i>Add new</a>
    <form id="Add-Career" class="form-horizontal hidden" method="post" action="<?php echo site_url() ?>/admin/careerPost">
        <fieldset>
            <legend>Add New Career</legend>
            <div class="control-group">
                <label class="control-label" for="job">Job Title</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="job" name="job" required="required" placeholder="System Analyst"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="div">Division</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="div" name="div" required="required" placeholder="IT"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="comp">Company</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="comp" name="comp" placeholder="Cognizant Technology" required="required" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="qual">Qualification</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="qual" name="qual" placeholder="B.Tech/B.E Civil" required="required" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="loc">Location</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="loc" name="loc" placeholder="Kochi, Banglore" required="required" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="cnum">Contact Number</label>
                <div class="controls">
                    <input type="tel" class="input-xlarge" id="cnum" name="cnum" placeholder="+91944 125 4563" pattern="\+?\d{10,15}" required="required"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="cemail">Email</label>
                <div class="controls">
                    <input type="email" class="input-xlarge" id="cemail" name="cemail" placeholder="career@example.org" required="required" />
                </div>
            </div>												
            <div class="control-group">
                <label class="control-label" for="details">Details</label>
                <div class="controls">
                    <textarea class="input-xlarge" id="details" name="details" rows="4" maxlength="3000"></textarea>
                </div>
            </div>			
            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Create"/> <input class="btn" type="reset" value="Cancel"/>
            </div>
        </fieldset>
    </form>


    <div class="vc_blog-list">



        <div class="vc_splitter"> <span class="bg"> </span> </div>
        <?php
        if (empty($careers)): {
                echo '<div class="alert alert-info">No posts to display</div>';
            } else:
            ?>
            <?php foreach ($careers as $new): ?>
                <article class="blog-row clearfix">



                    <div class="blog-right">

                        <div class="entry-date">

                            <div class="day"><?php echo date('d', strtotime($new['careerDate'])); ?></div>

                            <div class="month"><?php echo date('M', strtotime($new['careerDate'])); ?></div>

                        </div>

                        <div class="title">

                            <h3> <a href="#">  <?php echo $new['jobtitle'] ?> </a> </h3>

                            <span class="comments"><a href="#">Division: <?php echo $new['division'] ?></a> &nbsp;<a  class="delete-post" href="  <?php echo site_url() . '/admin/delCareer/' . $new['careerId']; ?>"> <i class="icon-trash"> </i>  Delete</a></span> </div>

                        <div class="description">

                            <p> <?php echo $new['description'] ?> <!--<a href="blog-single.html" class="vc_read-more"> read more </a>--> </p>

                        </div>

                    </div>

                </article>
                <div class="vc_splitter"> <span class="bg"> </span> </div>
            <?php endforeach; ?>
            <?php
            echo $pages;
        endif;
        ?>
    </div><!--  end vc--->




</div>
<?php require_once 'footer.php'; ?>