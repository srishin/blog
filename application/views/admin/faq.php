<?php require_once 'home.php'; ?>              
<div class="span9">
    <h2> Latest <span class="vc_main-color"> FAQ </span> </h2>
    <?php
    if (!empty($error)) {
        echo '<div class="alert alert-error"> ' . $error . '<i class="icon-exclamation-sign"> </i></div>';
    }
    if (!empty($success)) {
        echo '<div class="alert alert-success"><i class="icon-ok-sign"></i> New blog post added successfully</div>';
    }
    if (!empty($delete)) {
        echo '<div class="alert alert-error"><i class="icon-ok-sign"></i> Deleted successfully</div>';
    }
    ?>
    <a class="toggle-link" href="#Add-News"><i class="icon-plus"></i>Add new</a>
    <form id="Add-News" class="form-horizontal hidden" method="post" action="<?php echo site_url() ?>/admin/addfaq" enctype="multipart/form-data">

        <fieldset>
            <legend>Add New Post</legend>
            <div class="control-group">
                <label class="control-label" for="title">Question</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="title" name="ques" required="required"/>
                </div>
            </div>
           								
            <div class="control-group">
                <label class="control-label" for="details">Answer</label>
                <div class="controls">
                    <textarea class="input-xlarge" id="details" name="ans" rows="4" maxlength="300"></textarea>
                </div>
            </div>	

            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Create"/> <input class="btn" type="reset" value="Cancel"/>
            </div>
        </fieldset>
    </form>


    <div class="vc_blog-list">



        <div class="vc_splitter"> <span class="bg"> </span> </div>
        <?php
        if (empty($faq)): {
                echo '<div class="alert alert-info">No questions to display</div>';
            } else:
            ?>
            <?php foreach ($faq as $new): ?>
                <div class="container-fluid">
                    <div class="span9 mini-image">
                        <div class="vc_blog-list">     
                            <article class="blog-row clearfix">

                                <div>
                                    <div class="entry-date">
                                        <div class="day"><?php echo date('d', strtotime($new['date'])); ?></div>
                                        <div class="month"><?php echo date('M', strtotime($new['date'])); ?></div>
                                    </div>
                                    <div class="title">
                                        <h3> <a href="#">  <?php echo $new['question'] ?> </a> </h3>

                                        <span class="comments"><a class="delete-post" href="<?php echo site_url() . '/admin/delFaq/' . $new['qiD']; ?>"><i class="icon-trash"></i> Delete</a> </span> </div>

                                    <div class="description">

                                        <p> <?php echo $new['answer'] ?> </p>

                                    </div>

                                </div>

                            </article>
                            <div class="vc_splitter"> <span class="bg"> </span> </div>
                        </div>
                    </div>

                </div>

            <?php endforeach; ?>
            <?php
            echo $pages;
        endif;
        ?>
    </div>
    <!--  end vc-->
</div>
<?php require_once 'footer.php'; ?>