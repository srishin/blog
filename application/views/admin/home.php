<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en" class="ie6 ielt7 ielt8 ielt9"><![endif]--><!--[if IE 7 ]><html lang="en" class="ie7 ielt8 ielt9"><![endif]--><!--[if IE 8 ]><html lang="en" class="ie8 ielt9"><![endif]--><!--[if IE 9 ]><html lang="en" class="ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--> 
<html lang="en"><!--<![endif]--> 
    <head>
        <meta charset="utf-8">
        <title><?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo base_url() ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/bootstrap-responsive.min.css" rel="stylesheet">

        <!--theme-->
        <link href='<?php echo base_url(); ?>css/theme.css' rel='stylesheet' type='text/css'>
        <link href='<?php echo base_url(); ?>css/theme-responsive.css' rel='stylesheet' type='text/css'>
        <link href='<?php echo base_url(); ?>css/site.css' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" type='text/css'>
        <link href="<?php echo base_url(); ?>plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type='text/css'>

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link rel="shortcut icon" href="<?php echo base_url() ?>img/ico/favicon.ico" type="image/x-icon">
        <!-- <link rel="icon" href="<?php echo base_url() ?>img/ico/favicon.ico" type="image/x-icon"> -->
<!--        <style>
            u {
                position: absolute;
                font: 15px verdana;
                color: red;
                /*padding: 7px;*/
                /*margin-left: 280px;*/
            }

            
        </style>-->
    </head>
    <body>
        <div class="container-fluid">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> 
                        <a class="brand" href="<?php echo site_url() ?>/admin">Occudiz</a>
                        <div class="nav-collapse">

                            <ul class="nav">
                                <li>
                                    <a href="<?php echo site_url() ?>/admin"><i class="icon-home"></i> Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url() ?>/admin/news"><i class="icon-envelope"></i> Posts</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url() ?>/admin/faq"><i class="icon-question-sign"></i> FAQ</a>
                                </li>
                            <!--     <li>
                                    <a href="<?php echo site_url() ?>/admin/career"><i class="icon-check"></i> Careers</a>
                                </li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i>  Portfolio<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo site_url() ?>/admin/portfolio"><i class="icon-user"></i> Images</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url() ?>/admin/video"><i class="icon-cog"></i> Video</a>
                                        </li>										
                                    </ul>
                                </li> -->




<!-- 
                                <li>
                                    <a href="<?php echo site_url() ?>/admin/testimonials"><i class="icon-anchor"></i> Testmonials</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url() ?>/admin/startups"><i class="icon-certificate"></i> Startups</a>
                                </li> -->
                            </ul>
                            <ul class="nav pull-right">
                                <li class="dropdown">
                                    <a href="<?php echo site_url() ?>/admin/profile" class="dropdown-toggle" data-toggle="dropdown">@<?php echo $this->session->userdata('name'); ?><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo site_url() ?>/admin/profile"><i class="icon-user"></i> Profile</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url() ?>/admin/settings"><i class="icon-cog"></i> Settings</a>
                                        </li>										
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo site_url() ?>/admin/logout">Logout</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="span3">
                    <div class="well" style="padding: 8px 0;">
                        <ul class="nav nav-list">
                            <li class="nav-header">
                                <?php echo $this->session->userdata('name'); ?>
                            </li>
                            <li>
                                <a href="<?php echo site_url() ?>/admin"><i class="icon-home"></i> Home</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url() ?>/admin/news"><i class="icon-envelope"></i> Posts</a>
                            </li>
                            
                                <li>
                                    <a href="<?php echo site_url() ?>/admin/faq"><i class="icon-question-sign"></i> FAQ</a>
                                </li>
                            <!--                            <li>
                                                            <a href="<?php echo site_url() ?>/admin/files"><i class="icon-file"></i> Files</a>
                                                        </li>-->
                    <!--         <li>
                                <a href="<?php echo site_url() ?>/admin/career"><i class="icon-check"></i> Careers</a>
                            </li> -->
                            <li class="divider">
                            <li class="nav-header">
                                Your Account
                            </li>
                            <li>
                                <a href="<?php echo site_url() ?>/admin/profile"><i class="icon-user"></i> Profile</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url() ?>/admin/settings"><i class="icon-cog"></i> Settings</a>
                            </li>

                            </li>
                        </ul>
                    </div>
                </div>


