<?php require_once 'home.php';?>              
<div class="span9">
                    <h1>
                        Messages
                    </h1>
    <?php echo $pages; ?>
                    <ul class="messages">
                        <?php if(empty($messages)){
                            echo '<li class="alert alert-info">No Messages to display</li>';
                        }
                        ?>
                        <?php foreach ($messages as $data):?>
                        <li class="well">
                            <h3><?php echo $data['name'];?></h3>
                            <p>
                               <?php echo $data['message'];?>
                            </p>
                            <p>Email :  <b><?php echo $data['email'];?></b></p>
                            <span class="meta">
                                Written <em>on </em><?php  echo date('jS M Y', strtotime($data['datetime']));?>
                                <span class="pull-right">
                                    <a class="delete-post" href="<?php echo site_url().'/admin/delMsg/'.$data['messageid'];?>"><i class="icon-trash"></i> Delete</a></span>
                            </span>
                        </li>
                        <?php endforeach; ?>
                    </ul>
<?php echo $pages; ?>
                </div>
<?php require_once 'footer.php';?>