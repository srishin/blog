<?php require_once 'home.php'; ?>              
<div class="span9">
    <h2> Latest <span class="vc_main-color"> Blog </span> </h2>
    <?php
    if (!empty($error)) {
        echo '<div class="alert alert-error"> ' . $error . '<i class="icon-exclamation-sign"> </i></div>';
    }
    if (!empty($success)) {
        echo '<div class="alert alert-success"><i class="icon-ok-sign"></i> New blog post added successfully</div>';
    }
    if (!empty($delete)) {
        echo '<div class="alert alert-error"><i class="icon-ok-sign"></i> Deleted successfully</div>';
    }
    ?>
    <a class="toggle-link" href="#Add-News"><i class="icon-plus"></i>Add new</a>
    <form id="Add-News" class="form-horizontal hidden" method="post" action="<?php echo site_url() ?>/admin/post" enctype="multipart/form-data">

        <fieldset>
            <legend>Add New Post</legend>
            <div class="control-group">
                <label class="control-label" for="title">Title</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="title" name="title" required="required"/>
                </div>
            </div>
           
            <div class="control-group">
                <label class="control-label" for="image">Image</label>
                <div class="controls">
                    <input type="file" class="input-xlarge" id="image" name="image" required="required" />
                </div> 
            </div>												
            <div class="control-group">
                <label class="control-label" for="details">Details</label>
                <div class="controls">
                    <textarea class="input-xlarge" id="details" name="details" rows="4" maxlength="300"></textarea>
                </div>
            </div>	

            <div class="control-group">
                <label class="control-label" for="title">Title</label>
                <div class="controls">
                    <select type="text" class="input-xlarge" id="title" name="size" required="required">
                        <option value="small">Small</option>
                        <option value="medium">Medium</option>
                        <option value="large">Large</option>
                    </select>
                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="title">Color</label>
                <div class="controls">
                    <select type="text" class="input-xlarge" id="title" name="color" required="required">
                        <option value="#FF5722">Yellow</option>
                        <option value="#33B5E5">Blue</option>
                        <option value="#14A47A">Green</option>
                        <option value="#706699">Violet</option>
                    </select>
                </div>
            </div>		
            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Create"/> <input class="btn" type="reset" value="Cancel"/>
            </div>
        </fieldset>
    </form>


    <div class="vc_blog-list">



        <div class="vc_splitter"> <span class="bg"> </span> </div>
        <?php
        if (empty($news)): {
                echo '<div class="alert alert-info">No posts to display</div>';
            } else:
            ?>
            <?php foreach ($news as $new): ?>
                <div class="container-fluid">
                    <div class="span9 mini-image">
                        <div class="vc_blog-list">     
                            <article class="blog-row clearfix">

                                <div class="blog-left">

                                    <div class="vc_anim vc_anim-slide"> <a href="" class="vc_preview"> <img alt="example image" src="<?php echo base_url('uploads/posts') . '/' . $new['img'] ?>"  /> </a>


                                    </div>
                                </div>
                                <div class="blog-right">
                                    <div class="entry-date">
                                        <div class="day"><?php echo date('d', strtotime($new['date'])); ?></div>
                                        <div class="month"><?php echo date('M', strtotime($new['date'])); ?></div>
                                    </div>
                                    <div class="title">
                                        <h3> <a href="#">  <?php echo $new['title'] ?> </a> </h3>

                                        <span class="comments"><a class="delete-post" href="<?php echo site_url() . '/admin/delPost/' . $new['postId']; ?>"><i class="icon-trash"></i> Delete</a> </span> </div>

                                    <div class="description">

                                        <p> <?php echo $new['content'] ?> </p>

                                    </div>

                                </div>

                            </article>
                            <div class="vc_splitter"> <span class="bg"> </span> </div>
                        </div>
                    </div>

                </div>

            <?php endforeach; ?>
            <?php
            echo $pages;
        endif;
        ?>
    </div>
    <!--  end vc-->
</div>
<?php require_once 'footer.php'; ?>