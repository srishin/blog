<?php require_once 'home.php'; ?>              
<div class="span9">
    <h2> Portfolio <span class="vc_main-color"> Images </span> </h2>
    <?php
    if (!empty($error)) {
        echo '<div class="alert alert-error"> ' . $error . '<i class="icon-exclamation-sign"> </i></div>';
    }
    if (!empty($success)) {
        echo '<div class="alert alert-success"><i class="icon-ok-sign"></i> New news item was added successfully</div>';
    }
    if (!empty($delete)) {
        echo '<div class="alert alert-error"><i class="icon-ok-sign"></i> Deleted successfully</div>';
    }
    ?>
    <a class="toggle-link" href="#Add-image"><i class="icon-plus"></i>Add new</a>
    <form id="Add-image" class="form-horizontal hidden" method="post" action="<?php echo site_url() ?>/admin/addImage" enctype="multipart/form-data">

        <fieldset>
            <legend>Add New Portfolio</legend>
            <div class="control-group">
                <label class="control-label" for="title">Title</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="title" name="title" required="required"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="image">Image</label>
                <div class="controls">
                    <input type="file" class="input-xlarge" id="image" name="image" required="required" />
                </div> 
            </div>				
            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Create"/> <input class="btn" type="reset" value="Cancel"/>
            </div>
        </fieldset>
    </form>


    <div class="vc_blog-list">



        <div class="vc_splitter"> <span class="bg"> </span> </div>
        <?php
        if (empty($img)): {
                echo '<div class="alert alert-info"> No items to display</div>';
            } else:
            ?>
            <?php foreach ($img as $new): ?>
                <div class="span9 mini-image">
                    <div class="vc_blog-list">
                        <article class="blog-row clearfix">

                            <div class="blog-left">

                                <div class="vc_anim vc_anim-slide"> <a href="#" class="vc_preview"> <img alt="<?php echo $new['link']; ?>" src="<?php echo base_url('uploads/portfolio') . '/' . $new['link'] ?>"  /> </a>

                                    <!-- <div class="vc_hover">

                                        <div class="hover-wrapper">
                                            <div class="text-wrapper">

                                                <h4><?php echo $new['title']; ?> </h4>

                                            </div>

                                            <div class="icon-wrapper">

                                                <ul>

                                                    <li class="vc_icon"> <a data-rel="prettyPhoto" href="<?php echo base_url('uploads/portfolio') . '/' . $new['link'] ?>" > <i class="icon-zoom-in"> </i> </a> </li>

                                                    <li class="vc_icon"> <a  href="blog-single.html" > <i class="icon-link"> </i> </a> </li>

                                                </ul>

                                            </div>

                                        </div>

                                    </div> -->

                                </div>

                            </div>

                            <div class="blog-right">

                                <div class="entry-date">

                                    <div class="day"><?php echo date('d', strtotime($new['created'])); ?></div>

                                    <div class="month"><?php echo date('M', strtotime($new['created'])); ?></div>

                                </div>

                                <div class="title">

                                    <h3> <a href="#">  <?php echo $new['title'] ?> </a> </h3>

                                    <span class="comments">
                                        <a class="delete-post" href="<?php echo site_url() . '/admin/delPortfolio/' . $new['contentId']; ?>"><i class="icon-trash"></i> Delete</a></span> </div>


                                <div class="description">

                                    <p> <?php echo $new['details'] ?> <!--<a href="blog-single.html" class="vc_read-more"> read more </a>--> </p>

                                </div>

                            </div>

                        </article>
                        <div class="vc_splitter"> <span class="bg"> </span> </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php
            echo $pages;
        endif;
        ?>
    </div><!--  end vc--->
</div>
<?php require_once 'footer.php'; ?>
