<?php require_once 'home.php'; ?>
<div class="span9">
    <h1>
						Edit Your Profile
					</h1>
    <form id="edit-profile" class="form-horizontal" method="post" action="<?php  echo site_url();?>/admin/profileChange">
						<fieldset>
							<legend>Your Profile</legend>
							<div class="control-group">
								<label class="control-label" for="name">Name</label>
								<div class="controls">
                                                                    <input type="text" class="input-xlarge" id="name" name="name" value=" <?php echo $this->session->userdata('name'); ?>" required="required"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="phone">Phone</label>
								<div class="controls">
									<input type="text" class="input-xlarge" id="phone"  name="phone" value=" <?php echo $this->session->userdata('phone'); ?>" required="required" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="email">Email</label>
								<div class="controls">
									<input type="email" class="input-xlarge" id="email" name="email" value=" <?php echo $this->session->userdata('email'); ?>" required="required" />
								</div>
							</div>					
							<div class="form-actions">
                                                            <input type="submit" class="btn btn-primary" value="Save"/> <input class="btn" type="reset" value="Cancel" />
							</div>
						</fieldset>
					</form>
				</div>
<?php require_once 'footer.php'; ?>