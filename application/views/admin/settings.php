<?php require_once 'home.php'; ?>    
<div class="span9">
    <h1>
        Settings
    </h1>
    <form id="changePwd" class="form-horizontal" method="post" data-url="<?php echo site_url(); ?>/admin/">
        <fieldset>
            <legend>Change Your Password</legend>
            <div id="status"></div>
            <div class="control-group">
                <label class="control-label" for="current">Current Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="current" name="current"  required="required"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="new">New Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="new"  name="new"  required="required" />
                    <div class="help-inline" id="lenChk"><span class="text-info">Minimum six characters recommended</span></div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="match">Repeat Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="match" name="match"  required="required" />
                    <div class="help-inline" id="missMatch"></div>
                </div>

            </div>	
            
            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Save"/> <input class="btn" type="reset" value="Cancel" />
            </div>
        </fieldset>
    </form>
</div>
<?php require_once 'footer.php'; ?>    