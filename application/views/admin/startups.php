<?php require_once 'home.php'; ?>
<div class="span9">






    <h2> Startups <span class="vc_main-color">  </span> </h2>
    <?php
    if (!empty($error)) {
        echo '<div class="alert alert-error"> ' . $error . '<i class="icon-exclamation-sign"> </i></div>';
    }
    if (!empty($success)) {
        echo '<div class="alert alert-success"><i class="icon-ok-sign"></i> New news item was added successfully</div>';
    }
    if (isset($delete)) {
        echo '<div class="alert alert-error"><i class="icon-ok-sign"></i> Deleted successfully</div>';
    }
    ?>
    <a class="toggle-link" href="#Add-Startup"><i class="icon-plus"></i>Add new</a>
    <form id="Add-Startup" class="form-horizontal hidden" method="post" action="<?php echo site_url() ?>/admin/poststart" enctype="multipart/form-data">

        <fieldset>
            <legend>Add New Companies</legend>
            <div class="control-group">
                <label class="control-label" for="name">Company Name</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="name" name="name" placeholder="Enter name" required="required"/>
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label" for="website">Website</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="website" name="website" placeholder="Enter Website Address" required="required"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="logo">Logo</label>
                <div class="controls">
                    <input type="file" class="input-xlarge" id="logo" name="logo" required="required" />
                </div> 
            </div>	


            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Create"/> <input class="btn" type="reset" value="Cancel"/>
            </div>
        </fieldset>
    </form>


    <div class="vc_blog-list">



        <div class="vc_splitter"> <span class="bg"> </span> </div>
        <?php
        if (empty($start)): {
                echo '<div class="alert alert-info"> No items to display</div>';
            } else:
            ?>
            <?php foreach ($start as $new): ?>
                <div class="vc_team-expertise block">

                    <div class="wrapper">

                        <div class="container">

                            <div class="row-fluid">

                                <div id="vc_our-team-widget" class="span8">

                                    <div class="vc_blog-list">

                                        <div class="vc_splitter"> <span class="bg"> </span> </div>

                                        <article class="blog-row clearfix">

                                            <div class="blog-left">

                                                <div class="vc_row"> <img alt="example image" src="<?php echo base_url('uploads/startups') . '/' . $new['logo'] ?>" > </div>



                                            </div>

                                            <div class="blog-right clearfix">

                                                <div class="information vc_row">

                                                    <h4><?php echo $new['name']; ?> </h4>

                                                </div>
                                                <div>
                                                    <a style="padding: 3px" class="position vc_inverted" href="http://<?php echo $new['website']; ?>"><?php echo $new['website']; ?></a>
                                                </div>
                                                

                                                <div class="vc_address vc_row">


                                                    <div class="span3"> <a class="delete-post" href="<?php echo site_url() . '/admin/delStart/' . $new['startID']; ?>"><i class="icon-trash"></i> Delete</a></div>
                                                </div>

                                                <!--                                                <div class="description vc_row">
                                                
                                                                                                    <p> <?php echo $new['quote']; ?>  </p>
                                                
                                                                                                </div>-->

                                            </div>

                                        </article>        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php
            echo $pages;
        endif;
        ?>
    </div><!--  end vc--->
</div>

<?php require_once 'footer.php'; ?>