<?php require_once 'home.php'; ?>
<div class="span9">






    <h2> Testimonials <span class="vc_main-color">  </span> </h2>
    <?php
    if (!empty($error)) {
        echo '<div class="alert alert-error"> ' . $error . '<i class="icon-exclamation-sign"> </i></div>';
    }
    if (!empty($success)) {
        echo '<div class="alert alert-success"><i class="icon-ok-sign"></i> New news item was added successfully</div>';
    }
    if (isset($delete)) {
        echo '<div class="alert alert-error"><i class="icon-ok-sign"></i> Deleted successfully</div>';
    }
    ?>
    <a class="toggle-link" href="#Add-test"><i class="icon-plus"></i>Add new</a>
    <form id="Add-test" class="form-horizontal hidden" method="post" action="<?php echo site_url() ?>/admin/postTest" enctype="multipart/form-data">

        <fieldset>
            <legend>Add New News</legend>
            <div class="control-group">
                <label class="control-label" for="name">Name</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="name" name="name" placeholder="Enter name" required="required"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="designation">Designation</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="designation" name="designation" placeholder="Enter designation" required="required"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="quote">Quote</label>
                <div class="controls">
                    <textarea class="input-xlarge" id="quote" name="quote" rows="4" maxlength="300"></textarea>
                </div> 
            </div>
            <div class="control-group">
                <label class="control-label" for="image">Image</label>
                <div class="controls">
                    <input type="file" class="input-xlarge" id="image" name="image" required="required" />
                </div> 
            </div>	


            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Create"/> <input class="btn" type="reset" value="Cancel"/>
            </div>
        </fieldset>
    </form>


    <div class="vc_blog-list">



        <div class="vc_splitter"> <span class="bg"> </span> </div>
        <?php
        if (empty($test)): {
                echo '<div class="alert alert-info"> No items to display</div>';
            } else:
            ?>
            <?php foreach ($test as $new): ?>




<div class="span8">
    <div class="span2" style="padding:5px">
                        <img alt="example image" width="80" height="50" src="<?php echo base_url('uploads/testimonials') . '/' . $new['image'] ?>" > 
                    </div>
                <div class="accordion-heading ">
<!--                    -->

                    <a href="#" class="accordion-toggle"> 
                        <span class="subtitle">Name: <strong><?php echo $new['name']; ?></strong></span>&nbsp;&nbsp;&nbsp; <span style="font-size: 15px">Designation: <strong><?php echo $new['designation']; ?> </strong></span>  <br/>

                        <span class="subtitle">Quote: <strong><?php echo $new['quote']; ?> </strong></span></a>
                    <a class="delete-post" href="<?php echo site_url() . '/admin/delTest/' . $new['testID']; ?>">  <span class="subtitle"><i class="icon-trash"></i> Delete</span></a>

                
                    
                </div>
</div>










                <!--                <div class="vc_team-expertise block">
                
                                    <div class="wrapper">
                
                                        <div class="container">
                
                                            <div class="row-fluid">
                
                                                <div id="vc_our-team-widget" class="span8">
                
                                                    <div class="vc_blog-list">
                
                                                        <div class="vc_splitter"> </div>
                
                                                        <article class="blog-row clearfix">
                
                                                            <div class="blog-left">
                
                                                                <div class="vc_row"> <img alt="example image" src="<?php echo base_url('uploads/testimonials') . '/' . $new['image'] ?>" > </div>
                
                
                
                                                            </div>
                
                                                            <div class="blog-right clearfix">
                
                                                                <div class="information vc_row">
                
                                                                    <h4><?php echo $new['name']; ?> </h4>
                
                                                                    <span class="position vc_inverted"><?php echo $new['designation']; ?></span> </div>
                
                                                                <div class="vc_address vc_row">
                
                
                                                                    <div class="span3"> <a class="delete-post" href="<?php echo site_url() . '/admin/delTest/' . $new['testID']; ?>"><i class="icon-trash"></i> Delete</a></div>
                                                                </div>
                
                                                                <div class="description vc_row">
                
                                                                    <p> <?php echo $new['quote']; ?>  </p>
                
                                                                </div>
                
                                                            </div>
                
                                                        </article>        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
            <?php endforeach; ?>
            <?php
            echo $pages;
        endif;
        ?>
    </div><!--  end vc--->
</div>

<?php require_once 'footer.php'; ?>