<?php require_once 'home.php'; ?>              
<div class="span9">
    <h2> Portfolio <span class="vc_main-color"> Video </span> </h2>
    <?php
    if (!empty($error)) {
        echo '<div class="alert alert-error"> ' . $error . '<i class="icon-exclamation-sign"> </i></div>';
    }
    if (!empty($success)) {
        echo '<div class="alert alert-success"><i class="icon-ok-sign"></i> New news item was added successfully</div>';
    }
    if (!empty($delete)) {
        echo '<div class="alert alert-error"><i class="icon-ok-sign"></i> Deleted successfully</div>';
    }
    ?>
    <a class="toggle-link" href="#Add-Video"><i class="icon-plus"></i>Add new</a>
    <form id="Add-Video" class="form-horizontal hidden" method="post" action="<?php echo site_url() ?>/admin/addVideo" enctype="multipart/form-data">

        <fieldset>
            <legend>Add New video</legend>
            <div class="control-group">
                <label class="control-label" for="title">Title</label>
                <div class="controls">
                    <input type="text" class="input-xlarge" id="title" name="title" required="required"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="video">Video Code</label>
                <div class="controls">
                    <u id="uFirstName">lMymFYJWW5M</u> <input id="iFirstName" type="text" class="input-xlarge" id="video" name="video" placeholder="www.youtube.com/watch?v=" required="required"/>
                </div>                                                                                                                                                                                                               
            </div>  
            <!--div class="control-group">
                <label class="control-label" for="details">Details</label>
                <div class="controls">
                    <textarea class="input-xlarge" id="details" name="details" rows="4" maxlength="300"></textarea>
                </div>
            </div-->			
            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Create"/> <input class="btn" type="reset" value="Cancel"/>
            </div>
        </fieldset>
    </form>


    <div class="vc_blog-list">



        <div class=""> <span class="bg"> </span> </div>

        <?php foreach ($video as $new): ?>
            <div class="vc_promo block">

                <div class="row-fluid">

                    <div class="span7">

                        <div class="description">

                            <div >

                                <p class="subtitle">Name : <span class="vc_inverted"> <?php echo $new['title']; ?>  </span></p>

                                <p class="text"> Video Code: <strong class="vc_main-color"> <?php echo $new['link']; ?></strong> </p>
                                <p><span class="comments"><a class="delete-post" href="<?php echo site_url() . '/admin/delVideo/' . $new['videoID']; ?>"><i class="icon-trash"></i> Delete</a></span></p>

                            </div>

                        </div>

                    </div>
                    <div class="span4">
                        <object style="padding: 5px" width="276" height="169" data="http://www.youtube.com/v/<?php echo $new['link']; ?>">
                        </object>
                    </div>



                </div>

            </div>
        <?php endforeach; ?>
       <?php
            echo $pages;
//        endif;
        ?>
    </div><!--  end vc--->
</div>
<?php require_once 'footer.php'; ?>

<script>
    document.getElementById('iFirstName').onkeyup= function() {
        var u= document.getElementById('uFirstName');
        u.style.display= this.value>'' ? 'none':'inline';
    };
</script>