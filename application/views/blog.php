<!doctype html>
<html>
<head>
  <meta charset="utf-8">

  <!-- Site Title --> <meta property="og:title" content="Occudiz - Join Our Journey and Experience Code with Style..." />
  <meta property="og:type" content="company" />
  <meta property="og:url" content="http://www.occudiz.com/blog" /> 
  <meta property="og:site_name" content="Occudiz - Blog" />
  <meta property="og:description" content="Are you looking for powerful Javascript, Jquery driven websites, along with the benefits of open source platform technologies? Our specialist team of developers with expertise in PHP, HTML, HTML5, CSS3 and XHTML are skilled at customized web hosted solutions." />



  <meta property="og:image" content="http://www.occudiz.com/img/logo-big.png" />
  <meta property="og:image:secure_url" content="http://www.occudiz.com/img/logo-big.png" />
  <meta property="og:image:type" content="image/png" />
  <meta property="og:image:width" content="50" />
  <meta property="og:image:height" content="50" />
  <meta property="og:video" content="https://youtu.be/rRaccLGVz3w">
  
  <meta property="twitter:card" content="summary">
  <meta property="twitter:url" content="http://www.occudiz.com/blog">
  <meta property="twitter:title" content="Occudiz - Blog">
  <meta property="twitter:description" content="Are you looking for powerful Javascript, Jquery driven websites, along with the benefits of open source platform technologies? Our specialist team of developers with expertise in PHP, HTML, HTML5, CSS3 and XHTML are skilled at customized web hosted solutions.">
  <meta property="twitter:image" content="http://www.occudiz.com/img/logo-big.png">
  
  <meta name="author" content="Occudiz Tech Private Limited">
  <meta name="robot" CONTENT="index,follow">
  <meta name="language" CONTENT="English">
  <meta name="revisit-after" CONTENT="1"> 

  <title>Occudiz - Blog</title>

  <!-- Mobile Specific Meta Tag -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <!-- Favicon Icon -->
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">

  <!-- Theme Stylesheet -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" media="screen">

  <!-- Preloader (Pace.js) -->
  <script src="<?php echo base_url();?>assets/js/plugins/pace.min.js"></script>
  
  <!-- Modernizr -->
  <script src="<?php echo base_url();?>assets/js/libs/modernizr.custom.js"></script>
  <script src="<?php echo base_url();?>assets/js/libs/detectizr.min.js"></script>
  
</head>

<!-- Body -->
<body class="gray-bg">

    <!-- Preloader 
    <div id="preloader">
      <div class="logo">
        <img src="img/logo-big.png" alt="Occudiz ">
        <span> Occudiz </span>
      </div>
    </div>-->

    <!-- Fake scrollbar (when open popups/modals) -->
    <div class="fake-scrollbar"></div>

    <!-- Off-canvas Navigation -->
    <div class="offcanvas-nav">
      <!-- Head (Fixed Part) -->
      <div class="nav-head">
        <div class="top-bar">
          <!-- <form class="search-box">
            <span class="search-toggle waves-effect waves-light"></span>
            <input type="text" id="search-field">
            <button type="submit" class="search-btn"><i class="flaticon-search100"></i></button>
          </form>-->
          <div class="nav-close waves-effect waves-light waves-circle" data-offcanvas="close"><i class="flaticon-close47"></i></div>
          <div class="social-buttons">
            <a href="#" class="sb-twitter"><i class="bi-twitter"></i></a>
            <a href="#" class="sb-google-plus"><i class="bi-gplus"></i></a>
            <a href="#" class="sb-facebook"><i class="bi-facebook"></i></a>
          </div>
        </div>
        <a href="index.html" class="offcanvas-logo">
          <div class="icon"><center><img src="<?php echo base_url();?>assets/img/logo-big.png" alt="Occudiz  "><br><br> <span>Join Our Journey</span></center></div>
          
        </a>
        
      </div>
      <!-- Body (Scroll Part) -->
      <div class="nav-body">
        <div class="overflow">
          <div class="inner">
            <!-- Navigation -->
            <nav class="nav-link">
              <div class="scroll-nav">
                <ul>
                  <li><a href="http://www.occudiz.com/">Home</a></li>
                  
                  

                  <li><a class="scroll" href="http://www.occudiz.com/#features" data-offset-top="-5">Why Occudiz? </a></li>
                  <li><a class="scroll" href="http://www.occudiz.com/#one" data-offset-top="100">What we do?</a></li>
                  
                  
                  <li><a class="scroll" href="http://www.occudiz.com/#about" data-offset-top="100">About us
                  </a></li> 
                  <li><a class="scroll" href="http://www.occudiz.com/#video" data-offset-top="100">Join Our Journey
                  </a></li>  
                  <li><a class="scroll" href="http://www.occudiz.com/#contact" data-offset-top="100">Contact us
                  </a></li>      
                  
                  
                </div>
              </nav>
              <!-- Twitter/Occudiz FAQ Tabs -->
              <div class="offcanvas-tabs">
                
                
              </div>
              <!-- Instagram Posts -->
              <div class="offcanvas-instagram">
                <div class="instgr-row clearfix">
                  
                 
                </div>
                
                
              </div>
            </div>
          </div>
        </div>
      </div><!-- Off-canvas Navigation End -->

      <!--Modal (Signin/Signup Page)-->
      
      <!-- Navbar -->
      <header class="navbar navbar-fixed-top">
        <div class="container">
          <!-- Nav Toggle -->
          <div class="nav-toggle waves-effect waves-light waves-circle" data-offcanvas="open"><i class="flaticon-menu55"></i></div>
          <!-- Logo -->
          <a href="<?php echo site_url('blog/readblog');?>" class="logo">
            <img src="<?php echo base_url();?>assets/img/logo-big.png" alt="Occudiz blog">
            Blog
          </a>
          <!-- Toolbar -->
          <div class="toolbar">
          <!--<a href="#" class="btn btn-flat btn-light icon-left waves-effect waves-light"><i class="flaticon-download164"></i> Download</a>
          <a href="#" data-toggle="modal" data-target="#signin-page" data-modal-form="sign-in" class="action-btn">Sign in</a>-->
          <!-- Social Buttons -->
          <div class="social-buttons text-right">
            <a href="https://twitter.com/occudiztech" class="sb-twitter" target="a_blank"><i class="bi-twitter"></i></a>
            <a href="https://plus.google.com/110624192305291072992/posts" class="sb-google-plus" target="a_blank"><i class="bi-gplus"></i></a>
            <a href="https://www.facebook.com/occudiz" class="sb-facebook" target="a_blank"><i class="bi-facebook" ></i></a>
            <a href="https://www.linkedin.com/company/occudiz" class="sb-linkedin" target="a_blank"><i class="bi-linkedin" ></i></a>
          </div>
        </div>
      </div>
    </header><!-- Navbar End -->

    <!-- Page Heading -->
    <div class="page-heading text-right">
      <div class="container">
        <!--<form class="search-field form-control">
          <button type="submit" class="search-btn"><i class="flaticon-search100"></i></button>
          <input type="text" id="search-input">
          <label for="search-input">Search</label>
        </form>-->
        <h2>Blog</h2>
      </div>
    </div>
    
    <!-- Blog Grid -->
    <section class="space-top padding-bottom">
      <div class="container">
        <div class="masonry-grid">
          <div class="grid-sizer"></div>
          <div class="gutter-sizer"></div>
          <!-- Item -->


          <?php
          if (empty($news)): {
            echo '<div class="alert alert-info">No posts to display</div>';
          } else:
          ?>
          
          








          <?php foreach ($news as $new): ?>
            <div class="item <?php if($new['size'] == 'large') echo 'w2' ?>">
              <div class="post-tile">
                <a href="<?php echo site_url('blog/read/'.$new['postId'])?>" class="post-thumb waves-effect">
                  <img src="<?php echo base_url('uploads/posts') . '/' . $new['img'] ?>" alt="Post 1">
                </a>
                <div class="post-body" style="background-color:<?php echo $new['color'] ?>;opacity: 0.6;
                  filter: alpha(opacity=60);">
                  <div class="post-title" style="color: #0099ff">
                    <a href="<?php echo site_url('blog/read/'.$new['postId'])?>"><h3 style="color: #ffffff"><?php echo $new['title'] ?></h3></a>
                    <span style="color: #ffffff"><?php echo  (strlen($new['content']) > 103) ? substr($new['content'],0,100).'...    <a href="">Read More</a>' : $new['content']; ?></span>
                  </div>
                  <div class="post-meta">
                    <div class="column">
                    </div>
                    <div class="column text-right">
                      <span style="color: #ffffff"><?php echo date('F d Y', strtotime($new['date'])); ?></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <?php endforeach; ?>
          <?php echo $pages;
          endif;
          ?>


          <!-- Item -->
   



    </div>

    <br><br>
 <!-- Pagination 
        <div class="pagination space-top-3x space-bottom-3x">
          <div class="page-slider">
            <span></span>
            <input data-slider-id='ex1Slider' type="text" data-slider-min="1" data-slider-max="22" data-slider-step="1" data-slider-value="13"/>
          </div>
          <div class="controls">
            <a href="#">Older</a>
            <a href="#">Newer</a>
          </div>
        </div>-->
      </section><!-- Blog Grid End -->

      <!-- Footer -->
      <footer class="footer padding-top-3x" >
        
        <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d124945.63920479764!2d75.30137384703548!3d11.866654584884477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3ba422cc8e9a0bb1%3A0x90fe4cc2c619fe49!2soccudiz!3m2!1d11.8666632!2d75.37141439999999!5e0!3m2!1sen!2sin!4v1454574881842" style="position: absolute;width: 100%;height: 100%;" allowfullscreen></iframe>
        <div class="container space-top">

          <!-- Footer Head -->
          <a href="mailto:info@occudiz.com"><div class="footer-head-wrap">
            <a href="mailto:info@occudiz.com" class="footer-head waves-effect waves-button waves-float">
              <div class="logo">
                <img src="<?php echo base_url();?>assets/img/logo-big.png" alt="Occudiz">
              </div>
              <div class="info">
                <h2>Occudiz Tech private limited</h2>
                <div class="rating">
                  <i class="bi-star"></i>
                  <i class="bi-star"></i>
                  <i class="bi-star"></i>
                  <i class="bi-star"></i>
                  <i class="bi-star"></i>
                  
                </div>
                <!---Is this correct?-->
                <p>Kannur Technolodge
                  4th Floor, KK Trade Centre
                  Central Bus Terminal 
                  Thavakkara, Kannur - 670001</p>
                  <i class="fa fa-mail">info@occudiz.com</i>
                </div>
              </a>
            </div> </a>
            <div class="body padding-top-2x">
              <!-- Copyright -->
              <div class="column copyright">
                <p>2016 &copy; <a href="www.occudiz.com" target="_blank">Occudiz</a></p>
              </div>
              <!-- Gadget (Middle Column) -->
              <div class="column hidden-sm hidden-xs">
                <div class="gadget">
                </div>
              </div>
              <nav class="column footer-nav">
               
              </nav>
            </div>
          </div>
        </footer><!-- Footer End -->
        
        <!-- Javascript (jQuery) Libraries and Plugins -->
        <script src="<?php echo base_url();?>assets/js/libs/jquery-2.1.3.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/libs/jquery.easing.1.3.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/velocity.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/smoothscroll.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/waves.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/form-plugins.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/jquery.mCustomScrollbar.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-slider.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/scripts.js"></script>

      </body><!-- Body End-->
      </html>