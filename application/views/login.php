<html lang="en">

    <head>

        <meta charset="utf-8" />
        <title>Occudiz</title>
        <meta name="keywords" content="Kannur Technolodge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>img/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>img/ico/apple-touch-icon-57-precomposed.png">
        <!-- <link rel="shortcut icon" href="<?php echo base_url(); ?>img/ico/favicon.ico"> -->
        <!-- CSS -->

        <!-- Fonts CSS -->
        <link href="<?php echo base_url(); ?>css/fonts.css"  rel='stylesheet' type='text/css'>
        <!--[if lte IE 9]><link type='text/css' rel="stylesheet" href="<?php echo base_url(); ?>css/fonts/ie-google-webfont.css"><![endif]-->

        <!-- Bootstrap & FontAwesome CSS -->
        <link href='<?php echo base_url(); ?>css/bootstrap.min.css' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet" type='text/css'>
        <!--[if IE 7]><link type='text/css' rel="stylesheet" href="<?php echo base_url(); ?>css/font-awesome-ie7.min.css"><![endif]-->

        <!-- Plugin CSS -->

        <!-- Theme CSS -->
        <link href='<?php echo base_url(); ?>css/theme.css' rel='stylesheet' type='text/css'>

        <!-- Responsive CSS -->
        <link href='<?php echo base_url(); ?>css/bootstrap-responsive.min.css' rel='stylesheet' type='text/css'>
<!--        <link href='<?php echo base_url(); ?>css/theme-responsive.css' rel='stylesheet' type='text/css'>-->

        <!-- Design Mode CSS -->
<!--        <link id="link-design" href='<?php echo base_url(); ?>css/mode/mode-slash.css' rel='stylesheet' type='text/css'>
        <link id="link-color" href='<?php echo base_url(); ?>css/color/color-blue.css' rel='stylesheet' type='text/css'>-->

        <!-- Custom CSS -->
<!--        <link href='<?php echo base_url(); ?>custom/custom.css' rel='stylesheet' type='text/css'>-->

    </head>

    <body class="clearfix" data-smooth-scrolling="1">

        <div class="vc_body">

</div>
<div class="container">

    <div style="width:260px;margin: 1% auto;border: 1px solid #e0e0e0;padding:4% 3%;border-radius:2%; margin-top: 65px">
        <!--                login form-->
        <form class="login" id="loginform" data-url="<?php echo site_url('/login/doLogin') ?>">
            <h4 style="color:#0077b3;text-align: center">Login</h4>
            <div class="control-group input-prepend">
                <label for="login" class="control-label">User:</label>
                <span class="add-on">@</span>
                <input type="text" name="login" id="login" class="" placeholder="Username" required="required">
            </div>


            <div class="control-group input-prepend">
                <label for="password" class="control-label">Password:</label>
                <div class="controls">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input type="password" name="password" id="password" class="input-append" placeholder="Enter password" required="required">
                </div>
            </div>
            <div class="control-group" style="width:95%">
                <div id="status"></div>

            </div>
            <div class="control-group">
                <div>   
                    <button type="submit" class="btn btn-primary" >Login</button>&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="forgot-password"><a href="" class="forgot">Forgot your password?</a></span>
                </div>
            </div>
        </form>
        <!--              end  login form-->
        <form style="display: none;" class="login" id="forgotPwd" data-url="<?php echo site_url('/login/forgotPwd') ?>">
            <div class="control-group">
                <span>Enter email address added to your account
                    We'll send you an email with instructions to choose a new password. </span>
            </div>

            <div class="control-group input-prepend">
                <span class="add-on"><i class="icon-envelope"></i></span>
                <input type="text" name="login" id="mail" class="" placeholder="name@example.com" required="required">
            </div>
            <div class="control-group" style="width:95%">
                <div id="fgtStatus"></div>

            </div>
            <div class="control-group">
                <div> 
                    <button type="submit" class="btn btn-primary" >Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="forgot-password"><a href="" class="forgot">Back to login</a></span>
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.11.3.js"></script> 
    <script type="text/javascript" src="<?php echo base_url() ?>js/technolodge.login.js"></script> 
</div>
</body>
</html>
