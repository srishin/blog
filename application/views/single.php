<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <!-- SEO Meta Tags -->

<!--   <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-36056199-6', 'auto');
    ga('send', 'pageview');

  </script> -->

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Site Title -->
  <title>Occudiz</title>

  <!-- Mobile Specific Meta Tag -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Meta Properties -->
   <?php if($post): ?>
              <meta property="og:url"                content="<?php echo base_url().$_SERVER['REQUEST_URI'] ?>" />
              <meta property="og:type"               content="article" />
              <meta property="og:title"              content="<?php echo $post['title'] ?>" />
              <meta property="og:description"        content="<?php echo $post['content'] ?>" />
              <meta property="og:image"              content="<?php echo base_url('uploads/posts') . '/' . $post['img'] ?>" />

    <?php else:?>


  <?php endif;?>

  <!-- Favicon Icon -->
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">

  <!-- Theme Stylesheet -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" media="screen">

  <!-- Preloader (Pace.js) -->
  <script src="<?php echo base_url();?>assets/js/plugins/pace.min.js"></script>

  <!-- Modernizr -->
  <script src="<?php echo base_url();?>assets/js/libs/modernizr.custom.js"></script>
  <script src="<?php echo base_url();?>assets/js/libs/detectizr.min.js"></script>

</head>

<!-- Body -->
<!-- Remove '.fixed-footer' from body to disable fixed footer reveal on scroll -->
<body class="fixed-footer">




  <!-- Fake scrollbar (when open popups/modals) -->
  <div class="fake-scrollbar"></div>

  <!-- Off-canvas Navigation -->
  <div class="offcanvas-nav">
    <!-- Head (Fixed Part) -->
    <div class="nav-head">
      <div class="top-bar">
        <form class="search-box">
          <span class="search-toggle waves-effect waves-light"></span>
          <input type="text" id="search-field">
          <button type="submit" class="search-btn"><i class="flaticon-search100"></i></button>
        </form>
        <div class="nav-close waves-effect waves-light waves-circle" data-offcanvas="close"><i class="flaticon-close47"></i></div>
        <div class="social-buttons">
          <a href="https://twitter.com/occudiztech" class="sb-twitter" target="a_blank"><i class="bi-twitter"></i></a>
          <a href="https://plus.google.com/110624192305291072992/posts" class="sb-google-plus" target="a_blank"><i class="bi-gplus"></i></a>
          <a href="https://www.facebook.com/occudiz" class="sb-facebook" target="a_blank"><i class="bi-facebook" ></i></a>
          <a href="https://www.linkedin.com/company/occudiz" class="sb-linkedin" target="a_blank"><i class="bi-linkedin" ></i></a>
        </div>
      </div>
      <a href="#" class="offcanvas-logo scrollup"><center>
        <div class="icon"><img src="<?php echo base_url();?>assets/img/logo-big.png" alt="Occudiz-logo" width="150px" height="130px">

          <br><br><span>Join Our Journey</span></div></center>

        </a>
      </div>
      <!-- Body (Scroll Part) -->
      <div class="nav-body">
        <div class="overflow">
          <div class="inner">
            <!-- Navigation -->
            <nav class="nav-link">
              <div class="scroll-nav" id="scroll-nav">
                <ul>
                  <!-- Add ".scroll" class to anchor tag to enable smooth scrolling -->
                  <li class="active"><a class="scrollup" href="#">Home</a></li>

                  <li><a class="scroll" href="#features" data-offset-top="-5">Why Occudiz? </a></li>
                  <li><a class="scroll" href="#one" data-offset-top="100">What we do?</a></li>


                  <li><a class="scroll" href="#about" data-offset-top="100">About us
                  </a></li> 
                  <li><a class="scroll" href="#video" data-offset-top="100">Join Our Journey
                  </a></li>  
                  <li><a class="scroll" href="#contact" data-offset-top="100">Contact us
                  </a></li>      

                </div>
                <ul class="pages">

                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div><!-- Off-canvas Navigation End -->

      <!--Modal (Signin/Signup Page)-->
      <div class="modal fade" id="signin-page">
        <div class="modal-dialog">
          <div class="modal-form">
            <div class="tab-content">
              <!-- Sign in form -->
              <form class="tab-pane transition scale fade in active" id="signin-form" autocomplete="off">
                <h3 class="modal-title">Sign In</h3>
                <div class="form-control space-top-2x">
                  <input type="email" name="si_email" id="si_email" readonly onfocus="$(this).removeAttr('readonly');" required>
                  <label for="si_email">Email</label>
                  <span class="error-label"></span>
                  <span class="valid-label"></span>
                </div>
                <div class="form-control">
                  <input type="password" name="si-password" id="si-password" readonly onfocus="$(this).removeAttr('readonly');" required>
                  <label for="si-password">Password</label>
                  <a class="helper-link" href="#">Forgot password?</a>
                  <span class="error-label"></span>
                  <span class="valid-label"></span>
                </div>
                <label class="checkbox space-top-2x">
                  <input type="checkbox"> Remember me
                </label>
                <div class="clearfix modal-buttons">
                  <div class="pull-right">
                    <button type="button" class="btn btn-flat btn-default waves-effect" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-flat btn-primary waves-effect waves-primary">Sign In</button>
                  </div>
                  <!-- Switching forms (Fake nav tab) -->
                  <div class="form-switch pull-left"><a class="btn btn-flat btn-primary waves-effect waves-primary" href="#form-1">Sign Up</a></div>
                </div>
              </form>

              <!-- Sign up form -->
              <form class="tab-pane transition scale fade" id="signup-form">
                <h3 class="modal-title">Sign Up</h3>
                <div class="form-control space-top-2x">
                  <input type="email" name="su-email" id="su-email" required>
                  <label for="su-email">Email</label>
                  <span class="error-label"></span>
                  <span class="valid-label"></span>
                </div>
                <div class="form-control">
                  <input type="password" name="su-password" id="su-password" required>
                  <label for="su-password">Password</label>
                  <span class="error-label"></span>
                  <span class="valid-label"></span>
                </div>
                <div class="form-control">
                  <input type="password" name="su-password-repeat" id="su-password-repeat" required>
                  <label for="su-password-repeat">Repeat password</label>
                  <span class="error-label"></span>
                  <span class="valid-label"></span>
                </div>
                <label class="checkbox space-top-2x">
                  <input type="checkbox"> Subscribe for our news
                </label>
                <div class="clearfix modal-buttons">
                  <div class="pull-right">
                    <button type="button" class="btn btn-flat btn-default waves-effect" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-flat btn-primary waves-effect waves-primary">Sign Up</button>
                  </div>
                  <!-- Switching forms (Fake nav tab) -->
                  <div class="form-switch pull-left"><a class="btn btn-flat btn-primary waves-effect waves-primary" href="#form-2">Sign In</a></div>
                </div>
              </form>
            </div>
            <!-- Hidden real nav tabs -->
            <ul class="nav-tabs hidden">
              <li id="form-1"><a href="#signup-form" data-toggle="tab">Sign up</a></li>
              <li id="form-2"><a href="#signin-form" data-toggle="tab">Sign in</a></li>
            </ul>
          </div>
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

      <header class="navbar navbar-fixed-top">
        <div class="container">
          <!-- Nav Toggle -->
          <div class="nav-toggle waves-effect waves-light waves-circle" data-offcanvas="open"><i class="flaticon-menu55"></i></div>
          <!-- Logo -->
          <a href="<?php echo site_url('blog/readblog');?>" class="logo">
            <img src="<?php echo base_url();?>assets/img/logo-big.png" alt="Occudiz blog">
            Blog
          </a>
          <!-- Toolbar -->
          <div class="toolbar">
          <!--<a href="#" class="btn btn-flat btn-light icon-left waves-effect waves-light"><i class="flaticon-download164"></i> Download</a>
          <a href="#" data-toggle="modal" data-target="#signin-page" data-modal-form="sign-in" class="action-btn">Sign in</a>-->
          <!-- Social Buttons -->
          <div class="social-buttons text-right">
            <a href="https://twitter.com/occudiztech" class="sb-twitter" target="a_blank"><i class="bi-twitter"></i></a>
            <a href="https://plus.google.com/110624192305291072992/posts" class="sb-google-plus" target="a_blank"><i class="bi-gplus"></i></a>
            <a href="https://www.facebook.com/occudiz" class="sb-facebook" target="a_blank"><i class="bi-facebook" ></i></a>
            <a href="https://www.linkedin.com/company/occudiz" class="sb-linkedin" target="a_blank"><i class="bi-linkedin" ></i></a>
          </div>
        </div>
      </div>
    </header><!-- Navbar End -->

    <!-- Page Heading -->
    <div class="page-heading text-right">
      <div class="container">
        <form class="search-field form-control">
          <button type="submit" class="search-btn"><i class="flaticon-search100"></i></button>
          <input type="text" id="search-input">
          <label for="search-input">Search</label>
        </form>
        <h2>Single Post</h2>
      </div>
    </div>
    
    <!-- Single Post -->
    <section class="space-top padding-bottom-2x">
      <div class="container">
        <div class="row">
          <?php if($post): ?>

            <!-- Post Content -->
            <div class="col-lg-9 col-lg-push-3 col-sm-8 col-sm-push-4 padding-bottom">
              <div class="single-post box-float">
                <div class="inner">
                  <h1><?php echo $post['title'] ?></h1>
                  <img src="<?php echo base_url('uploads/posts') . '/' . $post['img'] ?>" class="space-top space-bottom-2x" alt="Appica2">
                  <div class="row">
                    <div class="col-md-9">
                      <p><?php echo $post['content'] ?></p>
                <!--     <h3 class="text-normal space-top">Thought-out UX</h3>
                    <p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                    <h3 class="text-normal space-top">Clean code</h3>
                    <p>Sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                    <figure class="space-bottom-2x">
                      <img src="img/blog/single02.png" class="space-top-2x" alt="Appica2">
                      <figcaption>Made by Bedismo</figcaption>
                    </figure>
                    <h3 class="text-normal space-top">Multiple Platforms</h3> -->
                    <!--  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</p> -->
                  </div>

                </div>
              </div>
              <div class="post-meta space-top-2x" style="background-color:<?php echo $post['color'] ?>">
                <div class="column"><?php echo date('F d, Y', strtotime($post['date'])); ?></div>
                <div class="column">
                  <div class="social-buttons text-right">
                    <a href="#comments" class="comment-count scroll" data-offset-top="160"><i class="flaticon-chat75"></i>42</a>
           <!--          <a href="#" class="sb-twitter"><i class="bi-twitter"></i></a>
                    <a href="#" class="sb-google-plus"><i class="bi-gplus"></i></a>
                    <a href="#" class="sb-facebook"><i class="bi-facebook"></i></a> -->
                    <div class="fb-share-button" 
                    data-href="<?php echo base_url().$_SERVER['REQUEST_URI'] ?>" 
                    data-layout="button_count">
                  </div>
                  <div class="fb-like" 
    data-href="<?php echo base_url().$_SERVER['REQUEST_URI'] ?>" 
    data-layout="button_count" 
    data-action="like" 
    data-show-faces="true">
  </div>
                </div>
              </div>
            </div>
            <div class="post-meta last-child space-bottom">
              <div class="column">
                <span>By <a href="#"><?php echo $post['poster'] ?></a>
                </div>
                <div class="column text-right">

                </div>
              </div>
              <!-- <div class="inner"> -->

              <!--   <div class="space-top space-bottom-2x" id="comments">
                  <h3 class="text-gray text-right text-light">Comments (42)</h3>
                </div> -->
                <!-- <div class="space-bottom-2x">
                  <div class="comment">
                    <div class="comment-meta">
                      <div class="column">
                        <div class="author">
                          <a href="#" class="ava"><img src="img/avatars/ava01.png" alt="Avatar"></a>
                          <span>by</span> <a href="#">Bedismo</a>
                        </div>
                      </div>
                      <div class="column text-right">
                        <span>17 hours ago</span>&nbsp;&nbsp;&nbsp;
                        <a href="#" class="reply-btn">Reply</a>
                      </div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                  </div>
                  <div class="comment comment-reply">
                    <div class="comment-meta">
                      <div class="column">
                        <div class="author">
                          <a href="#" class="ava"><img src="img/avatars/ava02.png" alt="Avatar"></a>
                          <span>by</span> <a href="#">Anna Frank</a>
                        </div>
                      </div>
                      <div class="column text-right">
                        <span>12 hours ago</span>&nbsp;&nbsp;&nbsp;
                        <a href="#" class="reply-btn">Reply</a>
                      </div>
                    </div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
                  </div>
                </div> -->
                <!-- </div> -->
             <!--  <a href="#" class="load-more waves-effect waves-light">
                <i class="flaticon-show5"></i>
                Show More (40)
              </a> -->
            </div>

            <!-- Comment Form -->
    <!--         <div class="box-float comments space-top">
              <h3 class="text-gray text-right text-light">Leave a comment</h3>
              <form id="comment-form" class="space-top space-bottom">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-control">
                      <input type="text" name="cf_name" id="cf_name" required>
                      <label for="cf_name">Name</label>
                      <span class="error-label"></span>
                      <span class="valid-label"></span>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-control">
                      <input type="email" name="cf_email" id="cf_email" required>
                      <label for="cf_email">Email</label>
                      <span class="error-label"></span>
                      <span class="valid-label"></span>
                    </div>
                  </div>
                </div>
                <div class="form-control">
                  <textarea name="cf_comment" id="cf_comment" required></textarea>
                  <label for="cf_comment">Comment</label>
                  <span class="error-label"></span>
                  <span class="valid-label"></span>
                </div>
                <div class="row space-top">
                  <div class="col-sm-6">
                    <label class="checkbox space-top">
                      <input type="checkbox"> Send replys to the comment
                    </label>
                  </div>
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-lg-6 col-lg-offset-6 col-md-8 col-md-offset-4 col-sm-12">
                        <button type="submit" class="btn btn-block btn-success btn-float waves-effect waves-light">Comment</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div> -->
          </div>
        <?php else: ?>
          <div class="col-lg-9 col-lg-push-3 col-sm-8 col-sm-push-4 padding-bottom">
            <div class="single-post box-float">
              <div class="inner">
                <h1>Sorry! The post you are searching is no longer available</h1>
                <br/>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <!-- Sidebar -->
        <div class="col-lg-3 col-lg-pull-9 col-sm-4 col-sm-pull-8">
          <div class="sidebar space-bottom-3x">
            <!-- Categories -->
            <div class="categories with-grid-btn">
              <a href="blog.html" class="grid-btn">
                <span></span>
                <span></span>
              </a>
              <ul>
                <li><a href="#">Design</a></li>
                <li><a href="#">HTML Coding</a></li>
                <li><a href="#">WordPress</a></li>
                <li><a href="#">Analytics</a></li>
                <li><a href="#">News</a></li>
              </ul>
            </div>
            <hr>


            <!-- Featured Posts -->
            <div class="box-float">
              <?php if($news): 
              $colors = array('bg-primary','bg-info','bg-warning','bg-success','bg-info');
              ?>

              <?php foreach($news as $r=>$rel):?>
               <a href="<?php echo site_url('blog/read/'.$rel['postId'])?>" class="featured-post bg-success waves-effect waves-light <?php echo $colors[($r)] ?>">
                <div class="content">
                  <div class="arrow"><i class="flaticon-right244"></i></div>
                  <h3><?php echo $rel['title'] ?></h3>
                  <p><?php echo substr($rel['content'],0,100) ?></p>
                </div>
              </a>

            <?php endforeach; ?>
          <?php endif;?>


        </div>
      </div>
    </div>
  </div>
</div>
</section><!-- Single Post End -->

<!-- Footer -->
<section >

  <footer class="footer padding-top-3x" >

    <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d124945.63920479764!2d75.30137384703548!3d11.866654584884477!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3ba422cc8e9a0bb1%3A0x90fe4cc2c619fe49!2soccudiz!3m2!1d11.8666632!2d75.37141439999999!5e0!3m2!1sen!2sin!4v1454574881842" style="position: absolute;width: 100%;height: 100%;" allowfullscreen></iframe>
    <div class="container space-top">

      <!-- Footer Head -->
      <a href="mailto:info@occudiz.com"><div class="footer-head-wrap">
        <a href="mailto:info@occudiz.com" class="footer-head waves-effect waves-button waves-float"><center>
          <div class="logo"><center>
            <img src="<?php echo base_url();?>assets/img/logo-big.png" alt="Occudiz" width="150px" height="150px"></center>
          </div></center>
          <div class="info">
            <h2>Occudiz Tech private limited</h2>
            <div class="rating">
              <i class="bi-star"></i>
              <i class="bi-star"></i>
              <i class="bi-star"></i>
              <i class="bi-star"></i>
              <i class="bi-star"></i>

            </div>
            <!---Is this correct?-->
            <p>Kannur Technolodge
              4th Floor, KK Trade Centre
              Central Bus Terminal 
              Thavakkara, Kannur - 670001</p>
              <i class="fa fa-mail">info@occudiz.com</i>
            </div>
          </a>
        </div> </a>
        <div class="body padding-top-2x">
          <!-- Copyright -->
          <div class="column copyright">
            <p>2016 &copy; <a href="www.occudiz.com" target="_blank">Occudiz</a></p>
          </div>
          <!-- Gadget (Middle Column) -->
          <div class="column hidden-sm hidden-xs">
            <div class="gadget">
            </div>
          </div>
          <nav class="column footer-nav">

          </nav>
        </div>
      </div>
    </footer><!-- Footer End -->
    
    <!-- Javascript (jQuery) Libraries and Plugins -->
    <script src="<?php echo base_url();?>assets/js/libs/jquery-2.1.3.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/libs/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/waves.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/form-plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/scripts.js"></script>

    <div id="fb-root"></div>
<!--   <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script> -->

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1590469601170584',
        xfbml      : true,
        version    : 'v2.6'
      });
    };

    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));


//   FB.ui(
//   {
//     method: 'share',
//     href: 'https://developers.facebook.com/docs/',
//   },
//   // callback
//   function(response) {
//     if (response && !response.error_message) {
//       alert('Posting completed.');
//     } else {
//       alert('Error while posting.');
//     }
//   }
// );
</script>

</body><!-- Body End-->
</html>