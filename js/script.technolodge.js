/* 
 * Technolodge Kannur
 */
//*************** Newsletter Subscription **********************//
$('form#newsletter').on('submit', function (e) {
    e.preventDefault();
    var item = $(this);
    if ($('#email').hasClass('valid'))
        $.ajax({
            type: "post",
            url: item.data('url') + '/home/subscribe',
            data: {email: $('#email').val()},
            dataType: 'json',
            success: function (res) {
                if (res.status) {
                    resStat('#subStat', 'alert alert-success', ' Thank you for subscribing ', 'icon-ok-sign');
                } else {
                    resStat('#subStat', 'alert alert-error', ' Enter valid values', 'icon-exclamation-sign');
                }
            }
        });
});
//******************* Response Status ************************//
function resStat(ID, cls, txt, ico) {
    $(ID).removeClass()
            .text('')
            .addClass(cls)
            .text(txt)
            .next('i').remove();
    $('<i></i>', {
        class: ico
    }).prependTo(ID);
}
//********************* Mail Us ********************************//
$('form#contact-form').on('submit', function (e) {
    e.preventDefault();
    $('div#success').addClass('hidden');
    $('div#empty').addClass('hidden');
    $('div#unexpected').addClass('hidden');
    var dataSent = {name: $('#contact-form-name').val(),
        email: $('#contact-form-email').val(),
        sub: $('#contact-form-subject').val(),
        message: $('#contact-form-message').val()};
    for (var prop in dataSent) {
        if (dataSent[prop] === "") {
            $('div#contact-form-result').show(500);
            $('div#empty').removeClass('hidden');
            return;
        }
    }
    $.ajax({
        type: "post",
        url: $(this).data('url'),
        data: dataSent,
        dataType: 'json',
        success: function (r) {
            if (parseInt(r.status) ==1) {
                $('div#contact-form-result').show(500);
                $('div#success').removeClass('hidden');
            } else {
                $('div#contact-form-result').show(500);
                $('div#unexpected').removeClass('hidden');
            }
        }
    });
});