$('form#changePwd').on('submit', function (e) {
    e.preventDefault();
    if (pMatch())
    {
        data = {pwd: $('#current').val()};
        $.ajax({
            type: "post",
            data: data,
            dataType: 'json',
            url: $('form#changePwd').data('url') + 'pwdCheck',
            success: function (res) {
                if (parseInt(res.valid)) {
                    $.ajax({
                        type: "post",
                        data: {new : $('#new').val()},
                        dataType: 'json',
                        url: $('form#changePwd').data('url') + 'pwdChange',
                        success: function (res) {
                            if (res.status) {
                                resStat('#status', 'alert alert-success', ' Password changed successfully', 'icon-ok-sign');
                            }
                            else {
                                resStat('#status', 'alert alert-danger', ' Error occured. Please try again', 'icon-remove-sign');
                            }
                        }
                    });

                } else {
                    resStat('#status', 'alert alert-danger', ' Password you typed doesn\'nt match current password', 'icon-remove-sign');
                }
            }
        });
    }
    else {
        resStat('#missMatch', 'help-inline text-error', 'Passwords do not match', 'icon-remove');
    }

});

$('#match').on('keyup', function () {
    pMatch();
});

$('#new').on('keyup', function () {
    if ($(this).val().length < 6) {
        resStat('#lenChk', 'help-inline text-warning', 'Too short', 'icon-remove');
        $(this).parent().addClass('control-group error');
    } else {
        resStat('#lenChk', 'help-inline text-success', 'Too short', 'icon-ok');
        $(this).parent().removeClass('control-group error');
    }
});


function pMatch() {
    var match = $('#match');
    if (match.val() === $('#new').val()) {
        match.parent()
                .removeClass('error')
                .addClass('success');
        return 1;
    } else
    {
        match.parent().addClass('control-group error');
        return 0;
    }
}


function resStat(ID, cls, txt, ico) {
    $(ID).removeClass()
            .text('')
            .addClass(cls)
            .text(txt)
            .next('i').remove();
    $('<i></i>', {
        class: ico
    }).prependTo(ID);
}

$('.toggle-link').click(function(e) {
			var target = $($(this).attr('href')).toggleClass('hidden');
			$.scrollTo(target);
			e.preventDefault();
		
		});
