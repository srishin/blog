
$('form#changePwd').on('submit', function (e) {
    e.preventDefault();
    if (pMatch())
    {
        data = {pwd: $('#current').val()};
        $.ajax({
            type: "post",
            data: data,
            dataType: 'json',
            url: $('form#changePwd').data('url') + 'pwdCheck',
            success: function (res) {
                if (parseInt(res.valid)) {
                    $.ajax({
                        type: "post",
                        data: {new : $('#new').val()},
                        dataType: 'json',
                        url: $('form#changePwd').data('url') + 'pwdChange',
                        success: function (res) {
                            if (res.status) {
                                resStat('#status', 'alert alert-success', ' Password changed successfully', 'icon-ok-sign');
                            }
                            else {
                                resStat('#status', 'alert alert-danger', ' Error occured. Please try again', 'icon-remove-sign');
                            }
                        }
                    });

                } else {
                    resStat('#status', 'alert alert-danger', ' Password you typed doesn\'t match current password', 'icon-remove-sign');
                }
            }
        });
    }
    else {
        reply('text-error', 'Error!', '#missMatch', 'icon-remove icon-white error');
    }

});

$('#match').on('keyup', function () {
    pMatch();
});

$('#new').on('keyup', function () {
    if ($(this).val().length < 6) {
        reply('text-warning', 'Too short', '#lenChk', 'icon-remove');
        $(this).parent().addClass('control-group error');
    } else {
        reply('text-success', ' Strong', '#lenChk', 'icon-ok');
        $(this).parent().removeClass('control-group error');
    }
});


function pMatch() {
    var match = $('#match');
    if (match.val() === $('#new').val()) {
        match.parent()
                .removeClass('error')
                .addClass('success');
        return 1;
    } else
    {
        match.parent().addClass('control-group error');
        return 0;
    }
}

function reply(className, text, toID, icon) {
    $('div.help-inline').children().replaceAll('span', 'icon');
    $('<i></i>', {
        class: icon
    }).appendTo(toID);
    $('<span></span>', {
        class: className,
        id: 'resMsg',
        text: text
    }).appendTo(toID);


}

function resStat(ID, cls, txt, ico) {
    $(ID).removeClass()
            .text('')
            .addClass(cls)
            .text(txt)
            .next('i').remove();
    $('<i></i>', {
        class: ico
    }).prependTo('#status');
}

//****************** Login **********************
$('form#loginform').on('submit', function (e) {
    e.preventDefault();
    dataSent = {name: $('#login').val(), pwd: $('#password').val()};
    $.ajax({
        type: "post",
        url: $(this).data('url'),
        data: dataSent,
        dataType: 'json',
        success: function (res) {
            if (res.url !== 'false') {
                location = res.url;
            }
            else {
                resStat('#status', 'alert alert-error', ' Invalid user credentials', 'icon-exclamation-sign');
            }
        }
    });
});

//****************** Toggle forgot *****************//
$('a.forgot').on('click', function (e) {
    e.preventDefault();
    $('form.login').toggle('500');
});

//****************** Forgot Password ***********************//
$('form#forgotPwd').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(this).data('url'),
        type: 'post',
        data: {mail: $('#mail').val()},
        dataType: 'json',
        success: function (r) {
            if (r.status) {
                resStat('#fgtStatus', 'alert alert-success', ' Please check your mail', 'icon-ok-sign');
            } else {
                resStat('#fgtStatus', 'alert alert-error', ' Invalid email id', 'icon-exclamation-sign');
            }

        }
    });
});